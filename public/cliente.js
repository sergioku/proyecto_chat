var socket=io();
$(document).ready(function(){
    $('#modalAcceder').modal({ backdrop: 'static', keyboard: false });
    $("#modalAcceder").modal("show");
});
$('#form2').submit(function(){
    if($('#username').val().trim()!== '') {
        $('#messages').empty().css("display", "block");
        socket.emit('meConecto', $('#username').val());
        $('#username').val('').focus();
        $("#modalAcceder").modal("hide");
        //$("#form").css("display", "inline");
        $(".navbar").css("display", "inline");
        $('#mensaje').focus();
        return false;
    }
    else{
        alert("El campo no debe estar vacio");
        $('#username').val('').focus();
    }
    return false;
});

$('#form').submit(function(){
    if($('#mensaje').val().trim()!== '') {
        socket.emit('mensaje', $('#mensaje').val());
        $('#mensaje').val('').focus();
        return false;
    }
    return false;
});


socket.on('mensaje', function (msg) {
    if (msg.pos === "der") {
        $elSpan=$('<span>').text("Tú: " + msg.message);
        $elSpan.css("background-color", "rgba(255, 255, 160, 0.8)");
    }
    else{
        $elSpan=$('<span>').text(msg.username + ": " + msg.message);
        $elSpan.css("background-color", "rgba(109, 255, 0, 0.8)");
    }
    $elList=$('<li>').append($elSpan);
    if (msg.pos === "der")
        $elList.css("text-align", "right");
    $('#messages').append($elList);
    $("body").animate({scrollTop: $('body')[0].scrollHeight}, 100);
});

socket.on('info', function (msg) {
    $elList = $('<li>').text(msg.message);
    $('#messages').append($elList);
    $elList.css("text-align", "center");
    $elList.css("background-color", "aqua");
    $elList.css("font-size", "20px");
    $elList.css("-webkit-box-shadow", "0px 0px 14px -1px rgba(82,125,255,1)");
    $elList.css("-moz-box-shadow", "0px 0px 14px -1px rgba(82,125,255,1)");
    $elList.css("box-shadow", "0px 0px 14px -1px rgba(82,125,255,1)");
    $elList.css("border-radius", "20px");
    $elList.css("margin-bottom", "30px");
    $("body").animate({scrollTop: $('body')[0].scrollHeight}, 100);
});