//ESTABLECEMOS EN UN VARIABLE NUESTRO PUERTO A UTILIZAR
var puerto=1994;

//LE INDICAMOS QUE VAMOS A UTILIZAR LA LIBRERIA EXPRESS
//PARA INSTALARLA, DESDE CONSOLA HACEMOS "npm install express --save"
var express = require("express");

//CREAMOS NUESTRA APLICACION
var app = express();

//INCLUIMOS LA LIBRERIA "path".
//LA INSTALAMOS DESDE LA CONSOLA, EN LA CARPETA DE NUESTRO PROYECTO "npm install path --save"
var path=require("path");

//INCLUIMOS LA LIBRERIA "socket.io"
//LA INSTALAMOS DESDE LA CONSOLA, EN LA CARPETA DE NUESTRO PROYECTO "npm install socket.io --save"
var socket=require("socket.io");


//COMENZAMOS AHORA CON LA PROGRAMACION ORIENTADA A EVENTOS
//ESTO ES QUE CUANDO OCURRA UNA COSA, LLAMARA A UNA FUNCION, Y ASI SUCESIVAMENTE


//CUANDO SE CONECTEN A MI SERVIDOR, LE ENVIAMOS ALGO
app.get("/", function(request, respuesta){
    console.log("Venga que esto empieza");
    respuesta.sendFile(path.join(__dirname+"/public/index.html"));
});
app.use(express.static("public"));

//LE INDICAMOS EL PUERTO POR EL QUE ESCUCHARA NUESTRA APP, PERO EN ESTE CASO CON SOCKET
//ESTO SIEMPRE IRA AL FINAL DE ESTE DOCUMENTO
var socketServer=socket.listen(app.listen(process.env.PORT || puerto));
console.log("El servidor está escuchando en el puerto "+puerto);

var $numUser=1;
var $usuarios=[];
socketServer.on('connection', function(socket){
    $numUser+=1;

    socket.on('meConecto', function(user){
        console.log('Se ha conectado un usuario');
        console.log('Usuario nuevo: ' + user);
        socket.user=user;
        $usuarios.push(user);

        socket.broadcast.emit('info', {
            message: 'Se ha conectado el usuario "' + socket.user + '". Otro mas a dar que hacer'
        });

        socket.emit('info', {
            message: 'Bienvenido usuario "' + socket.user + '". Venga ahi y empieza a cascar'
        });
    });

    socket.on('mensaje', function(msg){
        console.log('mensajico recibido: ' + msg);

        socket.broadcast.emit('mensaje', {
            username: socket.user,
            message: msg,
            pos: "izq"
        });

        socket.emit('mensaje', {
            username: socket.user,
            message: msg,
            pos: "der"
        });
    });


    socket.on('disconnect', function(){
        console.log('Pos se ha ido un usuario');
        socket.broadcast.emit('info',{
            message: 'Se ha desconectado el usuario  "'+socket.user+'". Rezemos por él...'
        });
        $numUser-=1;
    });
});

//PARA INICIAR EL SERVIDOR, EN CONSOLA HACEMOS "node index.js"

//AUNQUE HAY UNA APLICACION, QUE SE INSTALA POR CONSOLA, QUE RECARGA SOLA ESTE ARCHIVO
//ESTA APLICACION SE LLAMA "nodemon"
//LA INSTALAMOS ASI "npm install nodemon -g"
//LA PAGINA SE LANZARIA COMO "nodemon index.js"

//EN EL NAVEGADOR PONDRIAMOS "localhost:1994"